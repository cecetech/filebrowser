﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;
using System.Linq;
using TMPro;

public class FileBrowserDemo : MonoBehaviour
{
    //UI objects needed
    public Dropdown driveList;
    public GameObject folderlList;
    public GameObject fileList;
    public GameObject filePath;
    public Button textPrefab;
    public string currentPath;
    public string selectedPath;

    private List<string> dropdownOptions;
    private int menuIndex;
    private List<Dropdown.OptionData> menuOptions;

    public void startFileBrowser()
    {
        setUpFileBrowser();
    }

    public void setUpFileBrowser()
    {
        Debug.Log(Directory.GetCurrentDirectory());
    }

    public void getDriveList()
    {
        foreach(DriveInfo d in DriveInfo.GetDrives())
        {
            if (d.IsReady)
            {
                Debug.Log(d.Name);
                dropdownOptions.Add(d.Name);
            }

            driveList.ClearOptions();
            driveList.AddOptions(dropdownOptions);
        }
    }


    public static List<string> GetDirectories(string path, string searchPattern = "*",
        SearchOption searchOption = SearchOption.TopDirectoryOnly)
    {
        if (searchOption == SearchOption.TopDirectoryOnly)
            return Directory.GetDirectories(path, searchPattern).ToList();

        var directories = new List<string>(GetDirectories(path, searchPattern));

        for (var i = 0; i < directories.Count; i++)
            directories.AddRange(GetDirectories(directories[i], searchPattern));

        return directories;
    }

    private static List<string> GetDirectories(string path, string searchPattern)
    {
        try
        {
            return Directory.GetDirectories(path, searchPattern).ToList();
        }
        catch (Exception err)
        {
            Debug.Log(err);
            return new List<string>();
        }
    }

    public void getFolders()
    {
        Debug.Log(menuOptions[menuIndex].text);
        var directories = GetDirectories(menuOptions[menuIndex].text);
        foreach (string folder in directories)
        {
            Debug.Log(folder);
        }
        setFolderUI(directories);
    }

    public void getFolders(string path)
    {
        var directories = GetDirectories(path);
        foreach (string folder in directories)
        {
            Debug.Log(folder);
        }
        setFolderUI(directories);
    }


    public void setFolderUI(List<string> dirs)
    {
        GameObject content = GameObject.FindGameObjectWithTag("Content");

        foreach (Transform child in content.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        int height = 30;
        foreach (string folder in dirs)
        {
            height -= 30;
            Button tempObj = Instantiate(textPrefab, new Vector3(transform.position.x, transform.position.y + height,0), Quaternion.identity);
            tempObj.transform.parent = content.transform;
            tempObj.transform.position = new Vector3(50f, tempObj.transform.position.y, tempObj.transform.position.z);
            tempObj.GetComponentInChildren<Text>().text = folder;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        dropdownOptions = new List<string>();
        Debug.Log(Directory.GetCurrentDirectory());
        getDriveList();
    }

    // Update is called once per frame
    void Update()
    {
        //get the selected index
       menuIndex = driveList.value;

        //get all options available within this dropdown menu
        menuOptions = driveList.GetComponent<Dropdown>().options;

    }
}
