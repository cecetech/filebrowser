﻿using GracesGames.Common.Scripts;
using UnityEngine;

public class FileBrowserContainerInit : MonoBehaviour
{

    private FilleBrowserTest fileBrowser;

    private void SetupClickListeners()
    {
        // Hook up Directory Navigation methods to Directory Navigation Buttons
        Utilities.FindButtonAndAddOnClickListener("DirUpBtn", fileBrowser.DirectoryUp);
    }

    void Start()
    {
        InitLocalBrowser();
    }

    void InitLocalBrowser()
    {
        fileBrowser = FindObjectOfType<FilleBrowserTest>();
        SetupClickListeners();
    }
}
