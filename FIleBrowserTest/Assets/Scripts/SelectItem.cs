﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectItem : MonoBehaviour
{
   public Text buttonText;

    public void getButtonText()
    {
        buttonText = gameObject.GetComponentInChildren<Text>();
        FindObjectOfType<FileBrowserDemo>().getFolders(buttonText.text);
    }
}
