﻿using Dummiesman;
using System.IO;
using System.Text;
using UnityEngine;

public class ObjFromStream : MonoBehaviour
{
    public string GetObjectFromStream(string url)
    {
        //make www
        var www = new WWW(url);
        while (!www.isDone)
            System.Threading.Thread.Sleep(1);

        if (www.text.ToString().Trim() == "")
        {
            //error
            return "Unable to fetch file, check the url and try again";
        }
        //create stream and load
        var textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.text));
        var loadedObj = new OBJLoader().Load(textStream);
        return "Model imported successfully";
    }
}
