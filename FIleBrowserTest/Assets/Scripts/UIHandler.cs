﻿using GracesGames.Common.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{

    public Text path;
    public Text loadFilePath;
    public GameObject folderList;
    public GameObject fileList;

    public GameObject folderPrefab;
    public GameObject filePrefab;

    public GameObject browserSelector;
    public GameObject browserPanel;
    public GameObject alertPanel;
    public GameObject hud;


    public int startPosition = 50;

    public static int fileHeight = 50;
    public static int folderHeight = 50;

    private void Start()
    {

#if UNITY_WEBGL
               foreach (Transform child in browserSelector.transform)
        {
            if (child.name == "fileBrowserButton")
            {
                child.gameObject.SetActive(false);
            }
            if (child.name == "urlButton")
            {
                RectTransform rt = child.GetComponent<RectTransform>();
                rt.transform.localPosition = new Vector2(0, 0);
            }
        }
#endif
    }


    public void SetupPath(string path)
    {
        this.path.text = path;
    }

    public void resetFileBrowerPanels()
    {
        foreach (Transform child in folderList.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in fileList.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        fileHeight = 50;
        folderHeight = 50;

    }

    public void UpdateLoadFileText(string loadfiletext)
    {
        loadFilePath.text = loadfiletext;
    }

    public void SetupFolderButton(string folder)
    {
        GameObject temp = Instantiate(folderPrefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(folderList.transform, false);
        temp.GetComponentInChildren<Text>().text = folder;
        folderHeight += 50;
    }

    public void SetupFileButton(string file)
    {
        GameObject temp = Instantiate(filePrefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(fileList.transform, false);
        temp.GetComponentInChildren<Text>().text = file;
        fileHeight += 50;
    }

    public void AlertPanelDisplay(string message)
    {
        if (alertPanel)
        {
            alertPanel.SetActive(true);
            alertPanel.GetComponentInChildren<Text>().text = message;
        }
        else
        {
            Debug.Log("Need to set Alert Panel in UI Handler");
        }
    }

    public void AlertPanelClose()
    {
        alertPanel.SetActive(false);
    }




}
