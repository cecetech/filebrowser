﻿using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Dummiesman;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Collections;

public class FileLoadCaller : MonoBehaviour
{

    string path;
    public InputField urlPath;
    FilleBrowserTest FileBrowserScript;
    UIHandler uiScript;
    GameObject objectLoadPoint;
    public GameObject spawnPoint;

    public void LoadFile()
    {
        path = FileBrowserScript.getCurrentFilePath();
        if (string.IsNullOrEmpty(path))
        {
            // display error;
            uiScript.AlertPanelDisplay("Error importing file. Please try again");
        }
        else
        {
            if (objectLoadPoint != null)
                Destroy(objectLoadPoint);

            objectLoadPoint = new OBJLoader().Load(path);
            objectLoadPoint.transform.parent = spawnPoint.transform;
            objectLoadPoint.transform.position = spawnPoint.transform.position;
            Debug.Log(objectLoadPoint);
            uiScript.AlertPanelDisplay("File imported successfully: " + objectLoadPoint.name);
        }
    }

    public void Close()
    {
        FileBrowserScript.Close();
    }

    // Start is called before the first frame update
    void Start()
    {
        FileBrowserScript = GameObject.FindObjectOfType<FilleBrowserTest>();
        uiScript = GameObject.FindObjectOfType<UIHandler>();
    }


    public void LoadFileUrl()
    {
        string url = urlPath.text;
        StartCoroutine(GetRequest(url));
    }

    IEnumerator GetRequest(string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (string.IsNullOrEmpty(www.downloadHandler.text))
            {
                //error
                uiScript.AlertPanelDisplay("Unable to fetch file. Check the url and try again");
            }
            else
            {


                if (www.responseCode >= 200 && www.responseCode <= 300)
                {

                    if (objectLoadPoint != null)
                        Destroy(objectLoadPoint);

                    //create stream and load
                    string urlData = www.downloadHandler.text;
                    string[] urlFiles = urlData.Split('~');


                    if (urlFiles.Length < 2)
                    {
                        //url with just the file parameter
                        var fileStream = new MemoryStream(Encoding.UTF8.GetBytes(urlData));
                        objectLoadPoint = new OBJLoader().Load(fileStream);
                    }
                    else
                    {
                        var fileStream = new MemoryStream(Encoding.UTF8.GetBytes(urlFiles[0]));
                        var mtlStream = new MemoryStream(Encoding.UTF8.GetBytes(urlFiles[1]));
                        objectLoadPoint = new OBJLoader().Load(fileStream, mtlStream);
                    }

                    objectLoadPoint.transform.parent = spawnPoint.transform;
                    objectLoadPoint.transform.position = spawnPoint.transform.position;
                    uiScript.AlertPanelDisplay("File imported successfully ");
                }
                else
                {
                    uiScript.AlertPanelDisplay("Unable to fetch file. Check the url and try again");
                }
            }
        }
    }

}
