﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script to switch window panels (game objects) in Unity
/// </summary>
public class WindowSwitcher : MonoBehaviour
{
    [Header("Main screen hud")]
    public GameObject hud;
    [Header("Parent gameobject for windows")]
    public GameObject menuPanel;

    Stack windows = new Stack();

    /// <summary>
    /// Used to open a new window, call this function from an eventListener in the inspector and pass the window you want to open. The previous window will close automatically.
    /// Can be used as a next button handler in a linear window selection system 
    /// </summary>
    /// <param name="window"></param>
    public void OpenWindow(GameObject window)
    {
        if (windows.Count > 0)
        {
            GameObject currentWindow = (GameObject)windows.Peek();
            if (currentWindow != null)
                currentWindow.SetActive(false);
        }

        windows.Push(window);
        window.SetActive(true);
    }

    /// <summary>
    /// Closes current window and opens the previous one in the stack
    /// Can be used as a back button handler in a linear window selection system
    /// </summary>
    public void CloseWindow()
    {

        GameObject currentWindow = (GameObject)windows.Pop();
        currentWindow.SetActive(false);

        if (windows.Count > 0)
        {
            GameObject prevWindow = (GameObject)windows.Peek();
            if (prevWindow != null)
                prevWindow.SetActive(true);
        }

    }

    /// <summary>
    /// Clears window stack
    /// </summary>
    public void ClearWindows()
    {
        while (windows.Count > 0)
        {
            GameObject currentWindow = (GameObject)windows.Pop();
            currentWindow.SetActive(false);
        }
    }

    bool isToggle = true;
    /// <summary>
    /// Toggles the visibility of the hud and menu
    /// </summary>
    public void ToggleHud()
    {
        isToggle = !isToggle;
        hud.SetActive(isToggle);
        menuPanel.SetActive(!isToggle);
        ClearWindows();
    }

}
